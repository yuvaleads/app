<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Role;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
          'id'  =>  1,
          'title'=>'Admin',
          'description'=>'Admin Role in the system',
        ]);

        Role::create([
          'id'  =>  2,
          'title'=>'User',
          'description'=>'User Role in the system',
        ]);

        User::create([
            'name' => 'Admin',
            'email' => 'admin@test.com',
            'password' => Hash::make('admin'),
            'role_id'=> 1,
        ]);
        User::create([
            'name' => 'User',
            'email' => 'user@test.com',
            'password' => Hash::make('secret'),
            'role_id'=>2,
        ]);
    }
}
