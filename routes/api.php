<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->group(function () {
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
    Route::get('refresh', 'AuthController@refresh');

    Route::group(['middleware' => 'auth:api'], function(){
        Route::get('user', 'AuthController@user');
        Route::post('logout', 'AuthController@logout');
    });
});


Route::group(['middleware' => 'auth:api'], function(){

    Route::get('roles','RolesController@index')->middleware('admin');
    Route::post('roles/store','RolesController@store')->middleware('admin');
    Route::get('roles/{id}/edit','RolesController@edit')->middleware('admin');
    Route::put('roles/{id}/update','RolesController@update')->middleware('admin');

    Route::get('users','UserController@index')->middleware('admin');
    Route::get('users/{id}/show','UserController@show')->middleware('admin');
    Route::post('users/store','UserController@store')->middleware('admin');
    Route::get('users/{id}/edit','UserController@edit')->middleware('admin');
    Route::put('users/{id}/update','UserController@update')->middleware('admin');

    
});
